import pandas as pd

from datetime import datetime
from urllib.request import urlopen
from pathlib import Path
from itertools import islice

from django.utils.timezone import make_aware
from django.core.management.base import BaseCommand

from ...models import Yellow


class Command(BaseCommand):

    def handle(self, *args, **options):
        PROJECT_DIR = Path(__file__).resolve().parent.parent.parent.parent.parent
        url = 'https://s3.amazonaws.com/nyc-tlc/trip+data/yellow_tripdata_2020-01.csv'
        file_name = 'test.csv'
        if url.find('/'):
            file_name = url.rsplit('/', 1)[1]
        file_path = '{}/{}'.format(PROJECT_DIR, file_name)
        downloaded_file = Path(file_path)
        if not downloaded_file.is_file():
            response = urlopen(url)
            part_size = 16 * 1024
            with open(file_path, 'wb') as f:
                for part in iter(lambda: response.read(part_size), ''):
                    if not part:
                        break
                    f.write(part)

        data = pd.read_csv(file_path, chunksize=25000)

        for data_chunk in data:
            generator = self.create_instance(data_chunk.fillna(value=0).iterrows())
            for items in iter(lambda: list(islice(generator, 25000)), ''):
                if not items:
                    break
                Yellow.objects.bulk_create(items)

    def create_instance(self, csv_data):

        for i, data in csv_data:
            mapping_dict = {
                'vendor_id': data['VendorID'],
                'tpep_pickup_datetime': make_aware(datetime.strptime(data['tpep_pickup_datetime'], '%Y-%m-%d %H:%M:%S')),
                'tpep_dropoff_datetime': make_aware(datetime.strptime(data['tpep_dropoff_datetime'], '%Y-%m-%d %H:%M:%S')),
                'passenger_count': data['passenger_count'],
                'trip_distance': data['trip_distance'],
                'ratecode_id': data['RatecodeID'],
                'store_and_fwd_flag': data['store_and_fwd_flag'],
                'pu_location_id': data['PULocationID'],
                'do_location_id': data['DOLocationID'],
                'payment_type': data['payment_type'],
                'fare_amount': data['fare_amount'],
                'extra': data['extra'],
                'mta_tax': data['mta_tax'],
                'tip_amount': data['tip_amount'],
                'tolls_amount': data['tolls_amount'],
                'improvement_surcharge': data['improvement_surcharge'],
                'total_amount': data['total_amount'],
                'congestion_surcharge': data['congestion_surcharge'],
            }
            yield Yellow(**mapping_dict)
