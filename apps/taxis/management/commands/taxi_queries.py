from django.db.models import Count, Q, Avg, Sum, Max
from django.db import connection
from django.core.management.base import BaseCommand

from ...models import Yellow


class Command(BaseCommand):

    def handle(self, *args, **options):
        """Count the number of trips where the total amount is less than 5$."""
        under_5 = Yellow.objects.filter(total_amount__lt=5.00).only('id').count()
        print('under_5:', under_5)

        # """At what hour of the day do people spent the most money on taxi rides?"""
        most_common_hour = Yellow.objects.extra({'hour': "EXTRACT(HOUR from tpep_pickup_datetime)"}).values("hour").annotate(Sum('total_amount')).order_by('-total_amount__sum').first()
        print('most_common_hour:', most_common_hour)

        """What is average percentage of the tip?"""
        avg_tip_amount_percent = Yellow.objects.only('total_amount', 'tip_amount')\
            .aggregate(avg_total_amount_percent=(Avg('tip_amount') / Avg('total_amount')) * 100)['avg_total_amount_percent']
        print('avg_tip_amount_percent:', avg_tip_amount_percent)

        for c in connection.queries:
            print(c)

        # After one import:

        # 'SELECT COUNT(*) AS `__count` FROM `taxis_yellow` WHERE `taxis_yellow`.`total_amount` < 5', 'time': '1.551'

        """
            'SELECT (EXTRACT(HOUR from tpep_pickup_datetime)) AS `hour`, SUM(`taxis_yellow`.`total_amount`) AS `total_amount__sum` FROM
            `taxis_yellow` GROUP BY (EXTRACT(HOUR from tpep_pickup_datetime)) ORDER BY `total_amount__sum` DESC LIMIT 1', 'time': '3.486'
        """

        # 'SELECT ((AVG(`taxis_yellow`.`tip_amount`) / AVG(`taxis_yellow`.`total_amount`)) * 100) AS `avg_total_amount_percent` FROM `taxis_yellow`', 'time': '2.355'

        # After eleven imports:

        # Without indexing:

        # 'SELECT COUNT(*) AS `__count` FROM `taxis_yellow` WHERE `taxis_yellow`.`total_amount` < 5', 'time': '16.473'

        """
            'SELECT (EXTRACT(HOUR from tpep_pickup_datetime)) AS `hour`, SUM(`taxis_yellow`.`total_amount`) AS `total_amount__sum` FROM
            `taxis_yellow` GROUP BY (EXTRACT(HOUR from tpep_pickup_datetime)) ORDER BY `total_amount__sum` DESC LIMIT 1', 'time': '34.941'
        """

        # 'SELECT ((AVG(`taxis_yellow`.`tip_amount`) / AVG(`taxis_yellow`.`total_amount`)) * 100) AS `avg_total_amount_percent` FROM `taxis_yellow`', 'time': '24.155'

        # With indexing:

        # 'SELECT COUNT(*) AS `__count` FROM `taxis_yellow` WHERE `taxis_yellow`.`total_amount` < 5', 'time': '0.115'

        """
            'SELECT (EXTRACT(HOUR from tpep_pickup_datetime)) AS `hour`, SUM(`taxis_yellow`.`total_amount`) AS `total_amount__sum` FROM
            `taxis_yellow` GROUP BY (EXTRACT(HOUR from tpep_pickup_datetime)) ORDER BY `total_amount__sum` DESC LIMIT 1', 'time': '35.376'
        """
        # 'SELECT ((AVG(`taxis_yellow`.`tip_amount`) / AVG(`taxis_yellow`.`total_amount`)) * 100) AS `avg_total_amount_percent` FROM `taxis_yellow`', 'time': '24.181'
