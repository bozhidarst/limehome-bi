from django.db import models


class Yellow(models.Model):
    vendor_id = models.PositiveIntegerField()
    tpep_pickup_datetime = models.DateTimeField(db_index=True)
    tpep_dropoff_datetime = models.DateTimeField()
    passenger_count = models.PositiveIntegerField()
    trip_distance = models.DecimalField(max_digits=9, decimal_places=2)
    ratecode_id = models.PositiveIntegerField()
    store_and_fwd_flag = models.CharField(max_length=255)
    pu_location_id = models.PositiveIntegerField()
    do_location_id = models.PositiveIntegerField()
    payment_type = models.PositiveIntegerField()
    fare_amount = models.DecimalField(max_digits=9, decimal_places=2)
    extra = models.DecimalField(max_digits=9, decimal_places=2)
    mta_tax = models.DecimalField(max_digits=9, decimal_places=2)
    tip_amount = models.DecimalField(max_digits=9, decimal_places=2, db_index=True)
    tolls_amount = models.DecimalField(max_digits=9, decimal_places=2)
    improvement_surcharge = models.DecimalField(max_digits=9, decimal_places=2)
    total_amount = models.DecimalField(max_digits=9, decimal_places=2, db_index=True)
    congestion_surcharge = models.DecimalField(max_digits=9, decimal_places=2)

    def __str__(self):
        return f'Yellow - {self.id}'
