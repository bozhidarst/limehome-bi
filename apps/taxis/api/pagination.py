from rest_framework import pagination


class DefaultPagination(pagination.PageNumberPagination):
    page_size_query_param = 'size'
    page_size = 100
    max_page_size = 100
