from rest_framework import serializers

from ..models import Yellow


class YellowSerializer(serializers.ModelSerializer):

    class Meta:
        model = Yellow
        fields = '__all__'
