from rest_framework import decorators
from rest_framework import response as responses
from rest_framework.viewsets import ReadOnlyModelViewSet


from .filters import YellowFilterSet
from ..models import Yellow
from .pagination import DefaultPagination
from .serializers import YellowSerializer


class YellowViewSet(ReadOnlyModelViewSet):
    queryset = Yellow.objects.all()
    serializer_class = YellowSerializer
    pagination_class = DefaultPagination

    @decorators.action(detail=False)
    def count(self, request):
        self.filterset_class = YellowFilterSet
        count = (
            self.filter_queryset(self.get_queryset())
            .count()
        )

        return responses.Response(data={'yellow_count': count})
