from rest_framework.routers import DefaultRouter

from .viewsets import YellowViewSet

router = DefaultRouter()

router.register('yellow', YellowViewSet, 'yellow')
