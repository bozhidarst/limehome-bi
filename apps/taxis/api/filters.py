import django_filters as filters

from ..models import Yellow


class YellowFilterSet(filters.FilterSet):
    n = filters.NumberFilter(field_name='total_amount', lookup_expr='lt')

    class Meta:
        model = Yellow
        fields = ()
