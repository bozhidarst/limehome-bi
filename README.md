# Limehome BI

## First Part

> **Note:** My local database is **MySQL** with **InnoDB** storage engine. The storage engine can be easily switched to improve the running time. Also I was not sure if the task should be implemented with pure **Python** so I decided to go with **Django** to get the benefit from it's REST framework.

  - The download + loading script is in **apps/taxis/management/commands/import_yellow_taxis_data.py**
  > **Note:** If you have manually download the file, you should move it in the PROJECT_DIR's path
  **Example: /var/www/limehome-bi/FILE**
  - The queries themselves and the times are in the **apps/taxis/management/commands/taxi_queries.py**
  - I ran the queries in the **Google BigQuery** also. The images are in the **BASE_DIR**.
  - You can find the calculations in the **back_of_the_envelope_calculation.txt**

## Second Part

  - The url for the **number of trips where the total amount is under N** is **api/yellow/count/?n=NUMBER** 
  - **"Your data store is only able to handle up to 5 queries at once. Quickly describe how you would change (or if you would change) the design of your api to handle this limitation. Quickly describe how you would change (or if you would change) the design of your api to handle this limitation."**
 > Django REST Throttling secures that. If there is a need of changing the limitation I wouldn't go for less than 2requests/second on top of that we can run the requests on separate workers for further prevention.

## Third Part

  - My answers are in **streaming_answers.txt** file.